<?php
#setting
error_reporting(0);
include_once ('src/crest.php');
$botID = '14';# id бота
$hostForDeal = 'https://b24-89q6cg.bitrix24.ru/crm/deal/details/';# хост сделки
#Получаем дела
$paramsActivity = array('filter' => array('OWNER_TYPE_ID' => '2', 'STATUS' => '1'),
	'select' => array('ID', 'OWNER_ID', 'DEADLINE', 'RESPONSIBLE_ID')
);
$totalActivity = CRest::call('crm.activity.list', $paramsActivity);
$activityData = iteration($totalActivity['total'], 'crm.activity.list', $paramsActivity);
for ($i = 0, $size = count($activityData); $i < $size; $i++) {
	$activity[] = CRest::callBatch(array($i => $activityData[$i]));
}
#Получаем не закрытые сделки
$paramsDeals = array('filter' => array('CLOSED' => 'N'),
	'select' => array('ID', 'ASSIGNED_BY_ID')
);
$totalDeal = CRest::call('crm.deal.list', $paramsDeals);
$dealData = iteration($totalDeal['total'], 'crm.deal.list', $paramsDeals);
for ($i = 0, $size = count($dealData); $i < $size; $i++) {
	$deals[] = CRest::callBatch(array($i => $dealData[$i]));
}

#ищем сделки с просроченными делами
for ($i = 0, $size = count($activity); $i < $size; $i++) {
	for ($y = 0, $s = count($activity[$i]['result']['result'][$i]); $y < $s; $y++) {
		$delay = getDelay(explode('T', $activity[$i]['result']['result'][$i][$y]['DEADLINE'])[0]);
		if ($delay > 0) {
			$delayArr[$activity[$i]['result']['result'][$i][$y]['RESPONSIBLE_ID']][] = $activity[$i]['result']['result'][$i][$y]['OWNER_ID'];
		}
	}
}
#К ответственному привязываем все сделки
for ($i = 0, $size = count($deals); $i < $size; $i++) {
	for($y = 0, $s = count($deals[$i]['result']['result'][$i]); $y < $s; $y++) {
		$dealsArr[$deals[$i]['result']['result'][$i][$y]['ASSIGNED_BY_ID']][] = $deals[$i]['result']['result'][$i][$y]['ID'];
	}
}

#К ответственному привязываем все сделки с делами
for ($i = 0, $size = count($activity); $i < $size; $i++){
	for ($y = 0, $s = count($activity[$i]['result']['result'][$i]); $y < $s; $y++){
		$activArr[$activity[$i]['result']['result'][$i][$y]['RESPONSIBLE_ID']][] = $activity[$i]['result']['result'][$i][$y]['OWNER_ID'];
	}
}

foreach ($dealsArr as $key => $value) {
	$z = 0;
 	foreach ($activArr as $k => $v) {
 		if ($key == $k) {$workArr[$key] = array_diff($value, $v); $z = 1;}
 	}
	if ($z == 0) {
		$workArr[$key] = $dealsArr[$key];
	}
}

foreach ($workArr as $k => $v) {
	if (empty($v)) unset($workArr[$k]);
}

# Cоздаем нарушение для сделок без дел
foreach ($workArr as $key => $value) {
	$value = array_values($value);
	for($i = 0, $size = count($value); $i < $size; $i++) {
		$bisprocData1[] = array('method' => 'lists.element.add', 'params' => array(
			'IBLOCK_TYPE_ID' => 'bitrix_processes',
			'IBLOCK_ID'      => '28',
			'ELEMENT_CODE'   => 'element1'.microtime(),
			'FIELDS' => array(
				'NAME'         => 'Отсутствие дел',
				'PROPERTY_102' => $key,
				'PROPERTY_104' => '1',
				'PROPERTY_106' => $value[$i]
			)
		));
		usleep(1);
	}
}
# Cоздаем нарушение для сделок с просроченными делами
foreach ($delayArr as $key => $value) {
	for ($i = 0, $size = count($value); $i < $size; $i++) {
		$bisprocData2[] = array('method' => 'lists.element.add', 'params' => array(
			'IBLOCK_TYPE_ID' => 'bitrix_processes',
			'IBLOCK_ID'      => '28',
			'ELEMENT_CODE'   => 'element1'.microtime(),
			'FIELDS' => array(
				'NAME'         => 'Просроченное дело',
				'PROPERTY_102' => $key,
				'PROPERTY_104' => '1',
				'PROPERTY_106' => $value[$i]
			)
		));
		usleep(1);
	}
}
$bisprocData = array_merge($bisprocData1, $bisprocData2);
if (count($bisprocData) > 50) $bisprocData = array_chunk($bisprocData, 50);
# 17:00 - предупреждаем; 19:00 - штрафуем
$appsConfig = array();
$auth = array();
if (file_exists('config.php')) {
	include_once ('config.php');
}
$configBot = $appsConfig['d777bc963ef2582db68622e7acff759d']['AUTH']['application_token'];

if ($_GET['time'] == '1700') {
	if (file_exists('bot.php')) {
		include_once('bot.php');
	}
	
	$messageOne = 'Сделки с отсутствующими делами: ';
	$messageTwo = 'Сделки с пропущенными делами: ';
	$arrDeals = array_to_string($workArr, $messageOne, $hostForDeal);
	$arrDelayDeals = array_to_string($delayArr, $messageTwo, $hostForDeal);
	
	foreach ($arrDeals as $key => $value) {
		$result1 = restCommand('imbot.message.add', array('DIALOG_ID' => $key, 'MESSAGE' => $value), $appsConfig[$configBot]['AUTH']);
	}
	foreach ($arrDelayDeals as $key => $value) {
		$result2 = restCommand('imbot.message.add', array('DIALOG_ID' => $key, 'MESSAGE' => $value), $appsConfig[$configBot]['AUTH']);
	}

} elseif ($_GET['time'] == '1900') {
	for ($i = 0, $size = count($bisprocData); $i < $size; $i++) {
		$update[] = CRest::callBatch($bisprocData[$i]);
	}
}

#functions
/*
* Функция считает разницу между введенной датой и текущим днем
* Если результат отриц => сделка не просроченна, иначе просрочена
* @var checkX - дата, тип str, формат Y-m-d
*/
function getDelay ($checkX) {
	$checkInX = explode('-', $checkX);
	$date = mktime(0,0,0, $checkInX[1],$checkInX[2],$checkInX[0]);
	$today  = mktime(0,0,0, date("m"),date("d"),date("Y"));
	$delay = $today - $date;
	return $delay;
}

/*
* Функция для сборки массива (для Batch-запроса)
* @var total - тип int, количество элементов в массиве, должно быть > 50,
* условие формирования пакета Batch
* @var method - метод bitrix api, тип str
* @var params - тип arr, сформированный массив для метода bitrix
* @var key - тип bool, сохраняем ключи массива или нет 
*/
function iteration ($total, $method, $params, $key = false) {
	$iteration = intval($total / 50) + 1;
	if ($iteration % 50 == 0) $iteration -= 1;
	for ($i = 0; $i < $iteration; $i++) {
		$start = $i * 50;
		$data[$i] = array(
			'method' => $method,
			'params' => array(
				'start' => $start,
			)
		);
		$data[$i]['params'] += $params;
	}
	if (count($data) > 50) $data = array_chunk($data, 50, $key);
	return $data;
}

function array_to_string ($arr, $message, $host){
	foreach ($arr as $k => $v) {
		$str = $message;
		for ($i = 0, $size = count($v); $i < $size; $i++) {
			$z = $i + 1;
			$str .= "\n".$z.') [URL='.$host.$v[$i].'/]'.$v[$i].'[/URL]';
		}
		$newArr[$k] = $str;
	}
	return $newArr;
}