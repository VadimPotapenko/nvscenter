<?php
#=================================== setting ================================================#
error_reporting(0);
include_once ('src/crest.php');
#============================================================================================#
################################# получаем дела ##############################################
$paramsActivity = array('filter' => array('OWNER_TYPE_ID' => '2', '!STATUS' => '2'),
	'select' => array('ID', 'OWNER_ID', 'DEADLINE', 'RESPONSIBLE_ID')
);

$totalActivity = CRest::call('crm.activity.list', $paramsActivity);
$activityData = iteration($totalActivity['total'], 'crm.activity.list', $paramsActivity);
for ($i = 0, $size = count($activityData); $i < $size; $i++) {
	$activity[] = CRest::callBatch(array($i => $activityData[$i]));
}

##############################################################################################
################################# хватаем сделки #############################################
$paramsDeals = array('filter' => array('CLOSED' => 'N'),
	'select' => array('ID', 'ASSIGNED_BY_ID')
);

$totalDeal = CRest::call('crm.deal.list', $paramsDeals);
$dealData = iteration($totalDeal['total'], 'crm.deal.list', $paramsDeals);
for ($i = 0, $size = count($dealData); $i < $size; $i++) {
	$deals[] = CRest::callBatch(array($i => $dealData[$i]));
}

##############################################################################################
############################# ищем просроченные сделки #######################################
for ($i = 0, $size = count($activity); $i < $size; $i++) {
	for ($y = 0, $s = count($activity[$i]['result']['result'][$i]); $y < $s; $y++) {
		$delay = getDelay(explode('T', $activity[$i]['result']['result'][$i][$y]['DEADLINE'])[0]);
		if ($delay > 0) {
			$delayArr[$y]['id'] = $activity[$i]['result']['result'][$i][$y]['OWNER_ID'];
			$delayArr[$y]['owner'] = $activity[$i]['result']['result'][$i][$y]['RESPONSIBLE_ID'];
		}
	}
}

#############################################################################################
############################ ищем не качественные сделки ####################################
for ($i = 0, $size = count($deals); $i < $size; $i++) {
	for($y = 0, $s = count($deals[$i]['result']['result'][$i]); $y < $s; $y++) {
		$dealsArr[$deals[$i]['result']['result'][$i][$y]['ASSIGNED_BY_ID']][] = $deals[$i]['result']['result'][$i][$y]['ID'];
	}
}
for ($i = 0, $size = count($activity); $i < $size; $i++){
	for ($y = 0, $s = count($activity[$i]['result']['result'][$i]); $y < $s; $y++){
		$activArr[$activity[$i]['result']['result'][$i][$y]['RESPONSIBLE_ID']][] = $activity[$i]['result']['result'][$i][$y]['OWNER_ID'];
	}
}

foreach ($dealsArr as $key => $value) {
 	foreach ($activArr as $k => $v) {
 		if ($key == $k) $workArr[$key] = array_diff($value, $v);
 	}
}
foreach ($workArr as $k => $v) {
	if (empty($v)) unset($workArr[$k]);
}

############################################################################################
#################### Cоздаем нарушение для сделок с пропущенными делами ####################
foreach ($workArr as $key => $value) {
	$value = array_values($value);
	for($i = 0, $size = count($value); $i < $size; $i++) {
		$bisprocData1[] = array('method' => 'lists.element.add', 'params' => array(
			'IBLOCK_TYPE_ID' => 'bitrix_processes',
			'IBLOCK_ID'      => '28',
			'ELEMENT_CODE'   => 'element1',
			'FIELDS' => array(
				'NAME'         => 'Отсутствие дел',
				'PROPERTY_102' => $key,
				'PROPERTY_104' => '1',
				'PROPERTY_106' => 'D'.$value[$i]
			)
		));
	}
}
if (count($bisprocData1) > 50) $bisprocData1 = array_chunk($bisprocData1, 50);

###########################################################################################
#################### Cоздаем нарушение для сделок с просроченными делами ##################
for ($i = 0, $size = count($delayArr); $i < $size; $i++) {
	$bisprocData2[] = array('method' => 'lists.element.add', 'params' => array(
		'IBLOCK_TYPE_ID' => 'bitrix_processes',
		'IBLOCK_ID'      => '28',
		'ELEMENT_CODE'   => 'element1',
		'FIELDS' => array(
			'NAME'         => 'Просроченное дело',
			'PROPERTY_102' => $delayArr[$i]['owner'],
			'PROPERTY_104' => '1',
			'PROPERTY_106' => 'D'.$delayArr[$i]['id']
		)
	));	
}
if (count($bisprocData2) > 50) $bisprocData2 = array_chunk($bisprocData2, 50);

###########################################################################################
############################### time to be fired ##########################################
//bot.php?event=PUBLISH&application_token=XXX&PARAMS[DIALOG_ID]=1&PARAMS[MESSAGE]=Hello!
if ($_GET['time'] == '1700') {
	foreach ($workArr as $key => $value) {
		$value = array_values($value);
		for ($i = 0, $size = count($value); $i < $size; $i++) {
			file_get_contents('http://test.zlataooo.ru/nvscenter.bitrix24.ru/bot/bot.php?event=PUBLISH&PARAMS[DIALOG_ID]='.$key.'&PARAMS[MESSAGE]=У вас [URL=https://nvscenter.bitrix24.ru/crm/deal/details/'.$value[$i].'/]сделки[/URL] без дела!');
		}
	}
	for ($i = 0, $size = count($delayArr); $i < $size; $i++) {
		file_get_contents('http://test.zlataooo.ru/nvscenter.bitrix24.ru/bot/bot.php?event=PUBLISH&PARAMS[DIALOG_ID]='.$delayArr[$i]['owner'].'&PARAMS[MESSAGE]=Некоторые ваши [URL=https://nvscenter.bitrix24.ru/crm/deal/details/'.$delayArr[$i]['id'].'/]сделки[/URL] просрочены!');
	}
} elseif ($_GET['time'] == '1900') {
	for ($i = 0, $size = count($bisprocData2); $i < $size; $i++) {
		$update[] = CRest::callBatch($bisprocData2[$i]);
	}
	for ($i = 0, $size = count($bisprocData1); $i < $size; $i++) {
		$update[] = CRest::callBatch($bisprocData1[$i]);
	}
}

#########################################################################################
#==================================== output ===========================================#
# echo '<pre>';
# print_r ($messageDelay);
# echo '</pre>';
#=======================================================================================#
################################### functions ###########################################
/*
* Функция считает разницу между введенной датой и текущим днем
* Если результат отриц => сделка не просроченна, иначе просрочена
|================================================================|
* @var checkX - дата, тип str, формат Y-m-d
*/
function getDelay ($checkX) {
	$checkInX = explode('-', $checkX);
	$date = mktime(0,0,0, $checkInX[1],$checkInX[2],$checkInX[0]);
	$today  = mktime(0,0,0, date("m"),date("d"),date("Y"));
	$delay = $today - $date;
	return $delay;
}

/*
* Функция для сборки массива (для Batch-запроса)
|===============================================|
* @var total - тип int, количество элементов в массиве, должно быть > 50,
* условие формирования пакета Batch
*
* @var method - метод bitrix api, тип str
* @var params - тип arr, сформированный массив для метода bitrix
* @var key - тип bool, сохраняем ключи массива или нет 
*/
function iteration ($total, $method, $params, $key = false) {
	$iteration = intval($total / 50) + 1;
	if ($iteration % 50 == 0) $iteration -= 1;
	for ($i = 0; $i < $iteration; $i++) {
		$start = $i * 50;
		$data[$i] = array(
			'method' => $method,
			'params' => array(
				'start' => $start,
			)
		);
		$data[$i]['params'] += $params;
	}
	if (count($data) > 50) $data = array_chunk($data, 50, $key);
	return $data;
}